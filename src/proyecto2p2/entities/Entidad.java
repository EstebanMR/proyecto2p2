/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.entities;

/**
 *
 * @author muril
 */
public class Entidad {

    
    protected int id;
    protected String Nombre;
    protected String correo;
    protected String telefono;
    protected String ubicacion;
    protected String direccion;
    protected String contraseña;
    protected String cedJuridica;
    protected boolean verificado;

    public Entidad() {
    }

    public Entidad(int id, String Nombre, String correo, String telefono, String ubicacion, String direccion, String contraseña, String cedJuridica, boolean verificado) {
        this.id = id;
        this.Nombre = Nombre;
        this.correo = correo;
        this.telefono = telefono;
        this.ubicacion = ubicacion;
        this.direccion = direccion;
        this.contraseña = contraseña;
        this.cedJuridica = cedJuridica;
        this.verificado = verificado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    
    
    public String getCedJuridica() {
        return cedJuridica;
    }

    public void setCedJuridica(String cedJuridica) {
        this.cedJuridica = cedJuridica;
    }

    public boolean isVerificado() {
        return verificado;
    }

    public void setVerificado(boolean verificado) {
        this.verificado = verificado;
    }

    
}
