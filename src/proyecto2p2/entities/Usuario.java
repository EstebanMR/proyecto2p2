/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.entities;

/**
 *
 * @author muril
 */
public class Usuario {

    private int id;
    private String Nombre;
    private String cedula;
    private String Apellidos;
    private String direccion;
    private String correo;
    private String telefono;
    private String ubicacion;
    private String contraseña;
    private boolean tipo;

    public Usuario() {
    }

    public Usuario(int id, String Nombre, String cedula, String Apellidos, String direccion, String correo, String telefono, String ubicacion, String contraseña, boolean tipo) {
        this.id = id;
        this.Nombre = Nombre;
        this.cedula = cedula;
        this.Apellidos = Apellidos;
        this.direccion = direccion;
        this.correo = correo;
        this.telefono = telefono;
        this.ubicacion = ubicacion;
        this.contraseña = contraseña;
        this.tipo = tipo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return Nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getContraseña() {
        return contraseña;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

}
