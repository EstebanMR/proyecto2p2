/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.dao;

import java.sql.Timestamp;
import java.sql.Connection;
import java.util.LinkedList;
import proyecto2p2.entities.Tramite;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author muril
 */
public class TramiteDAO {

    public LinkedList<Tramite> cargar() {
        LinkedList<Tramite> tramites = new LinkedList<>();
        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, codigo, nombre, descripcion, requisitos, precio, f_creacion, f_modificacion, activo\n"
                    + "	FROM public.tramite";
            PreparedStatement stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                tramites.add(cargarTramite(rs));
            }
            return tramites;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor/ " + e);
        }
    }

    private Tramite cargarTramite(ResultSet rs) throws SQLException {
        Tramite t = new Tramite();
        t.setId(rs.getInt("id"));
        t.setCodigo(rs.getString("codigo"));
        t.setNombre(rs.getString("nombre"));
        t.setDescripcion(rs.getString("descripcion"));
        t.setRequisitos(rs.getString("requisitos"));
        t.setPrecio(rs.getDouble("precio"));
        t.setFechaCreacion(rs.getDate("f_creacion"));
        t.setUltimaModificacion(rs.getDate("f_modificacion"));
        t.setActivo(rs.getBoolean("activo"));
        return t;
    }

    public void insert(Tramite tra) throws SQLException {
        try (Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO public.tramite(\n"
                    + " codigo, nombre, descripcion, requisitos, precio, f_creacion, f_modificacion, activo)\n"
                    + "	VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, tra.getCodigo());
            stmt.setString(2, tra.getNombre());
            stmt.setString(3, tra.getDescripcion());
            stmt.setString(4, tra.getRequisitos());
            stmt.setDouble(5, tra.getPrecio());
            stmt.setTimestamp(6, new Timestamp(tra.getFechaCreacion().getTime()));
            stmt.setTimestamp(7, new Timestamp(tra.getUltimaModificacion().getTime()));
            stmt.setBoolean(8, tra.isActivo());
            stmt.executeQuery();
        } catch (SQLException e) {
            throw new SQLException("Ha ocurrido un error/ " + e.getMessage());

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor/ " + e);
        }
    }

    public void activar(int id, boolean b) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "UPDATE public.tramite\n"
                    + "	SET activo=?\n"
                    + "	WHERE id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setBoolean(1, b);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void actualizar(Tramite t) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "UPDATE public.tramite\n"
                    + "	SET id=?, codigo=?, nombre=?, descripcion=?, requisitos=?, precio=?, f_creacion=?, f_modificacion=?, activo=?\n"
                    + "	WHERE id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, t.getId());
            ps.setString(2, t.getCodigo());
            ps.setString(3, t.getNombre());
            ps.setString(4, t.getDescripcion());
            ps.setString(5, t.getRequisitos());
            ps.setDouble(6, t.getPrecio());
            ps.setTimestamp(7, new Timestamp(t.getFechaCreacion().getTime()));
            ps.setTimestamp(8, new Timestamp(t.getUltimaModificacion().getTime()));
            ps.setBoolean(9, t.isActivo());
            ps.setInt(10, t.getId());
            ps.executeQuery();

        } catch (Exception e) {
            throw new RuntimeException("Error al actualizar tramite");
        }
    }
}
