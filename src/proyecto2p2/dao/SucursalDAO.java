/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import proyecto2p2.entities.sucursal;

/**
 *
 * @author muril
 */
public class SucursalDAO {

    public boolean insert(sucursal s) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO public.sucursal(\n"
                    + " id_entidad, num_sucursal, ciudad, ubicacion)\n"
                    + "	VALUES (?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, s.getId_sucursal());
            stmt.setInt(2, s.getNumSucursal());
            stmt.setString(3, s.getCiudad());
            stmt.setString(4, s.getUbicacion());
            return stmt.executeUpdate() > 0;
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor/ " + e);
        }
        return false;
    }

}
