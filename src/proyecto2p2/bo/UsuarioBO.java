/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.bo;

import proyecto2p2.dao.UsuarioDAO;
import proyecto2p2.entities.Usuario;

/**
 *
 * @author muril
 */
public class UsuarioBO {

    /**
     * llama a insertar el usuario
     * @param u usuario llenado con el registro
     * @param repass para verificar si las contraseñas son iguales
     * @return el estado del insert
     */
    public boolean guardar(Usuario u, String repass) {
        validar(u, repass);
        u.setContraseña(Util.getEncripted(u.getContraseña()));
        return new UsuarioDAO().insertar(u);

    }

    /**
     * verifica si los datos del login no son nulos y llama un método para solicitar los datos del usuario
     * @param u usuario llenado en el login
     * @return el usuario completo llenado con la consulta a la base de datos
     */
    public Usuario sesion(Usuario u) {
        try {
            if (u == null) {
                throw new RuntimeException("Datos inválidos, por favor intente de nuevo");
            } else if (u.getCorreo().isBlank() && u.getCedula().isBlank()) {
                throw new RuntimeException("La cédula o correo son datos requeridos");
            } else if (u.getContraseña().isBlank()) {
                throw new RuntimeException("La contraseña es un dato requerido");
            }
            u.setContraseña(Util.getEncripted(u.getContraseña()));
            return new UsuarioDAO().pedirUsuario(u);
        } catch (RuntimeException e) {
            throw new RuntimeException("se cayó en login usuario");
        }
    }

    /**
     * valida que los datos ingresados en el registro
     * @param u usuario llenado en el registro
     * @param repass una copia de la contraseña para verificar que sea correcta
     */
    private void validar(Usuario u, String repass) {
        if (u == null) {
            throw new RuntimeException("Datos inválidos!!");
        }

        if (!u.getContraseña().equals(repass)) {
            throw new RuntimeException("Las contraseñas no coinciden");
        }

        if (u.getContraseña().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        if (u.getCedula().trim().isEmpty()) {
            throw new RuntimeException("La cédula es requerida");
        }

        if (u.getNombre().trim().isEmpty()) {
            throw new RuntimeException("El nombre es requerido");
        }

        if (u.getApellidos().trim().isEmpty()) {
            throw new RuntimeException("El primer apellido es requerido");
        }

        if (u.getCorreo().isBlank()) {
            throw new RuntimeException("El correo es requerido");
        }

        if (!u.getCorreo().contains("@")) {
            throw new RuntimeException("El correo es requerido");
        }
    }

}
