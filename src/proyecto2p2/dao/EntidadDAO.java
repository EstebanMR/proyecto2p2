/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import proyecto2p2.entities.Entidad;

/**
 *
 * @author muril
 */
public class EntidadDAO {

    public boolean insertar(Entidad e) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO public.entidad(\n"
                    + "	 nombre, cedula_jur, direccion, correo, telefono, ubicacion, contrasena, activa)\n"
                    + "	VALUES ( ?, ?, ?, ?, ?, ?, ?, ?);";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, e.getNombre());
            stmt.setString(2, e.getCedJuridica());
            stmt.setString(3, e.getDireccion());
            stmt.setString(4, e.getCorreo());
            stmt.setString(5, e.getTelefono());
            stmt.setString(6, e.getUbicacion());
            stmt.setString(7, e.getContraseña());
            stmt.setBoolean(8, false);

            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new RuntimeException("Ah acurrido un error / " + ex);
        }
    }

    public Entidad pedirEntidad(Entidad e) {
        String pass = "";
        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, nombre, cedula_jur, direccion, correo, telefono, ubicacion, contrasena, activa\n"
                    + " FROM public.entidad WHERE contrasena = ? and (cedula_jur = ? or correo = ?)";

            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, e.getContraseña());
            pstmt.setString(2, e.getCedJuridica());
            pstmt.setString(3, e.getCorreo());
            ResultSet rs = pstmt.executeQuery();
            System.out.println("se ejecutó");
            if (rs.next()) {
                return preparar(rs);
            }
            return e;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        } catch (Exception ex) {
            throw new RuntimeException("Problemas de conexión con la base de datos/ " + e);
        }
        return null;
    }

    private Entidad preparar(ResultSet rs) {
        try {
            Entidad e = new Entidad();
            e.setId(rs.getByte("id"));
            e.setNombre(rs.getString("nombre"));
            e.setCedJuridica(rs.getString("cedula_jur"));
            e.setDireccion(rs.getString("direccion"));
            e.setCorreo(rs.getString("correo"));
            e.setTelefono(rs.getString("telefono"));
            e.setUbicacion(rs.getString("ubicacion"));
            e.setContraseña(rs.getString("contrasena"));
            e.setVerificado(rs.getBoolean("activa"));
            return e;
        } catch (SQLException e) {
            throw new RuntimeException("Problemas de conexión con la base de datos");

        }
    }

    public LinkedList<Entidad> cargar() {
        LinkedList<Entidad> ent = new LinkedList<>();
        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, nombre, cedula_jur, direccion, correo, telefono, ubicacion, contrasena, activa\n"
                    + "	FROM public.entidad";
            PreparedStatement stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ent.add(cargarEntidad(rs));
            }
            return ent;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor/ " + e);
        }
    }

    public void activar(int id, boolean b) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "UPDATE public.entidad\n"
                    + "	SET activa = ?\n"
                    + "	WHERE id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setBoolean(1, b);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private Entidad cargarEntidad(ResultSet rs) {
        Entidad e = new Entidad();
        try {
            e.setId(rs.getInt("id"));
            e.setNombre(rs.getString("nombre"));
            e.setCedJuridica(rs.getString("cedula_jur"));
            e.setDireccion(rs.getString("direccion"));
            e.setCorreo(rs.getString("correo"));
            e.setTelefono(rs.getString("telefono"));
            e.setUbicacion(rs.getString("ubicacion"));
            e.setVerificado(rs.getBoolean("activa"));
            return e;
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
