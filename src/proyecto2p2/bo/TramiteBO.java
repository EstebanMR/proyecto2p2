/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.bo;

import java.sql.SQLException;
import java.util.LinkedList;
import proyecto2p2.entities.Tramite;
import proyecto2p2.dao.TramiteDAO;

/**
 *
 * @author muril
 */
public class TramiteBO {

    public LinkedList<Tramite> cargarTramites() {
        return new TramiteDAO().cargar();
    }

    public void ingresar(Tramite tra) throws SQLException {
        new TramiteDAO().insert(tra);
    }

    public void actualizar(Tramite tra) {
        new TramiteDAO().actualizar(tra);
    }

    public void eliminar(int id) {
        try {
            if (id > 0) {
                new TramiteDAO().activar(id, false);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
