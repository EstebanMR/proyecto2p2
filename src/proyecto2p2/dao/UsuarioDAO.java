/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import proyecto2p2.entities.Usuario;

/**
 *
 * @author muril
 */
public class UsuarioDAO {

    /**
     * busca en la base de datos por el usuario en el login
     * @param u cuenta con la cedula o correo y la contraseña
     * @return el usuario completo
     */
    public Usuario pedirUsuario(Usuario u) {
        String pass = "";
        try (Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, nombre, apellidos, cedula, direccion, correo, telefono, ubicacion, contrasenna, tipo\n"
                    + " FROM public.usuario WHERE contrasenna = ? and (cedula = ? or correo = ?)";

            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, u.getContraseña());
            pstmt.setString(2, u.getCedula());
            pstmt.setString(3, u.getCorreo());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return preparar(rs);
            }
            return u;
        }catch(RuntimeException e){
            System.out.println(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con la base de datos");
        }
        return null;
    }

    /**
     * toma los datos que devuelve la consulta de la base de datos y los convierte en un usuario
     * @param rs resultado de la consulta a la base de datos
     * @return un usuario con base a la consulta
     */
    private Usuario preparar(ResultSet rs) {
        try {
            Usuario u = new Usuario();
            u.setId(rs.getByte("id"));
            u.setNombre(rs.getString("nombre"));
            u.setApellidos(rs.getString("apellidos"));
            u.setCedula(rs.getString("cedula"));
            u.setDireccion(rs.getString("direccion"));
            u.setCorreo(rs.getString("correo"));
            u.setTelefono(rs.getString("telefono"));
            u.setUbicacion(rs.getString("ubicacion"));
            u.setContraseña(rs.getString("contrasenna"));
            u.setTipo(rs.getBoolean("tipo"));
            return u;
        } catch (SQLException e) {
            throw new RuntimeException("Problemas de conexión con la base de datos");

        }
    }

    /**
     * inserta un usuario en la base de datos
     * @param u usuario a registrar
     * @return si se insertó con exito
     */
    public boolean insertar(Usuario u) {
        try(Connection con = Conexion.getConexion()){
            String sql = "INSERT INTO public.usuario(nombre, apellidos, cedula,"
                    + " direccion, correo, telefono, ubicacion, contrasenna, tipo) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, u.getNombre());
            stmt.setString(2, u.getApellidos());
            stmt.setString(3, u.getCedula());
            stmt.setString(4, u.getDireccion());
            stmt.setString(5, u.getCorreo());
            stmt.setString(6, u.getTelefono());
            stmt.setString(7, u.getUbicacion());
            stmt.setString(8, u.getContraseña());
            stmt.setBoolean(9, false);
            
            return stmt.executeUpdate()>0;
        } catch(Exception e){
            throw new RuntimeException("Ah acurrido un error / "+ e);
        }

    }
}
