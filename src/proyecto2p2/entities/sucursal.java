/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.entities;

/**
 *
 * @author muril
 */
public class sucursal  {

    private int id;
    private int id_sucursal;
    private int numSucursal;
    private String ciudad;
    private String ubicacion;
    

    public sucursal() {
    }

    public sucursal(int id, int id_sucursal, int numSucursal, String ciudad, String ubicacion) {
        this.id = id;
        this.id_sucursal = id_sucursal;
        this.numSucursal = numSucursal;
        this.ciudad = ciudad;
        this.ubicacion = ubicacion;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(int id_sucursal) {
        this.id_sucursal = id_sucursal;
    }
    
    public int getNumSucursal() {
        return numSucursal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setNumSucursal(int numSucursal) {
        this.numSucursal = numSucursal;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

}
