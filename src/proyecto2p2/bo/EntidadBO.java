/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.bo;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecto2p2.entities.Entidad;
import proyecto2p2.dao.EntidadDAO;
import proyecto2p2.entities.Usuario;

/**
 *
 * @author muril
 */
public class EntidadBO {

    public boolean guardarEntidad(Entidad e, String repass) {
        validar(e, repass);
        e.setContraseña(Util.getEncripted(e.getContraseña()));
        return new EntidadDAO().insertar(e);
    }

    private void validar(Entidad e, String repass) {
        if (e == null) {
            throw new RuntimeException("Datos inválidos!!");
        }

        if (!e.getContraseña().equals(repass)) {
            throw new RuntimeException("Las contraseñas no coinciden");
        }

        if (e.getContraseña().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        if (e.getCedJuridica().trim().isEmpty()) {
            throw new RuntimeException("La cédula es requerida");
        }

        if (e.getNombre().trim().isEmpty()) {
            throw new RuntimeException("El nombre es requerido");
        }

        if (e.getCorreo().isBlank()) { 
            throw new RuntimeException("El correo es requerido");
        }

        if (!e.getCorreo().contains("@")) {
            throw new RuntimeException("El correo es requerido");
        }
    }

    public Entidad sesion(Entidad e) {
        try {
            System.out.println(e);
            if (e == null) {
                throw new RuntimeException("Datos inválidos, por favor intente de nuevo");
            } else if (e.getCorreo().isBlank() && e.getCedJuridica().isBlank()) {
                throw new RuntimeException("La cédula o correo son datos requeridos");
            } else if (e.getContraseña().isBlank()) {
                throw new RuntimeException("La contraseña es un dato requerido");
            }
            e.setContraseña(Util.getEncripted(e.getContraseña()));
            return new EntidadDAO().pedirEntidad(e);
        } catch (RuntimeException ex) {
            throw new RuntimeException("se cayó en login entidad/ "+ ex);
        } catch (Exception ex){
                System.out.println(ex.getMessage());
        }
        return null;
    }

    public LinkedList<Entidad> cargarActualizar() {
        return new EntidadDAO().cargar();
    }

    public void activar(int id, boolean b) {
        try {
            if (id > 0) {
                new EntidadDAO().activar(id, b);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    
}
