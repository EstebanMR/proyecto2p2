/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p2.gui;


import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.core.SWTNativeInterface;
import java.awt.BorderLayout;

/**
 *
 * @author muril
 */
public class FrmBrowser extends javax.swing.JDialog {

    JWebBrowser navegador;
    private String url;

    /**
     * Creates new form FrmBrowser
     */
    public FrmBrowser(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setup();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpNavegador = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1080, 700));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jpNavegador.setMinimumSize(new java.awt.Dimension(1080, 600));
        jpNavegador.setPreferredSize(new java.awt.Dimension(1920, 1080));

        javax.swing.GroupLayout jpNavegadorLayout = new javax.swing.GroupLayout(jpNavegador);
        jpNavegador.setLayout(jpNavegadorLayout);
        jpNavegadorLayout.setHorizontalGroup(
            jpNavegadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1150, Short.MAX_VALUE)
        );
        jpNavegadorLayout.setVerticalGroup(
            jpNavegadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );

        getContentPane().add(jpNavegador, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1150, 580));

        jButton2.setText("Agregar ubicación");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(848, 620, 290, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * se encarga de tomar la URL y obtener las coordenandas de la ubicación y volver al registro
     * @param evt 
     */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        String url1 = navegador.getResourceLocation();
        String[] parts = url1.split("@");
        String part1 = parts[1];
        String[] parts1 = part1.split(",");
        String parte0 = parts1[0];
        String parte1 = parts1[1];
        url = parte0 + "," + parte1;
        System.out.println(url);
        
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmBrowser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmBrowser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmBrowser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmBrowser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmBrowser dialog = new FrmBrowser(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jpNavegador;
    // End of variables declaration//GEN-END:variables

    /**
     * se encarga de levantar la ventana y cargar lo necesario para el navegador
     */
    private void setup() {
        navegador = new JWebBrowser();
        SWTNativeInterface.initialize();
        SWTNativeInterface.open();
        this.setLocationRelativeTo(null);
        jpNavegador.setSize(800, 600);

        jpNavegador.setLayout(new BorderLayout());
        navegador.navigate("http://www.google.com/maps");
        navegador.setStatusBarVisible(false);
        navegador.setBarsVisible(false);
        navegador.setButtonBarVisible(false);
        navegador.setLocationBarVisible(false);
        jpNavegador.add(navegador);
    }

    /**
     * devuelve la URL obtenida a la ventana de registro
     * @return url
     */
    public String getUrl() {
        return this.url;
    }
}
